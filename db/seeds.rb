# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Product.delete_all
Product.create(:title => 'Learn Web Development with Rails',
    :description => 
    %{
        Ruby on Rails Tutorial book and screencast series 
        teach you how to develop and deploy real, 
        industrial-strength web applications with Ruby on Rails.
    },
    :image_url => 'https://scontent.fsgn2-4.fna.fbcdn.net/v/t1.6435-0/p640x640/167884433_3322074478019102_2796807840667389872_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=825194&_nc_ohc=jH5ra4L9tjgAX_jE_qU&_nc_ht=scontent.fsgn2-4.fna&tp=6&oh=0f04a65dd5b7a1569137641060becf6f&oe=6094B89E',
    :price => 29.99)
  
Product.create(:title => 'The Ruby Programming Language',
    :description =>
    %{
        The Ruby Programming Language is the authoritative guide 
        to Ruby and provides comprehensive coverage 
        of versions 1.8 and 1.9 of the language.
    },
    :image_url => 'https://scontent.fsgn2-3.fna.fbcdn.net/v/t1.6435-9/168437538_276299717324214_444641895723159338_n.jpg?_nc_cat=108&ccb=1-3&_nc_sid=825194&_nc_ohc=OlgIYE3r4joAX92QHDj&_nc_ht=scontent.fsgn2-3.fna&oh=818ac816f29d53cb7333dfcda418aa42&oe=60956342',
    :price => 39.99)